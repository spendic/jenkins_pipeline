package com.comprigo.skyking.util;

import com.comprigo.skyking.model.api.FlightQuery;
import com.comprigo.skyking.model.api.FlightResult;
import com.comprigo.skyking.model.kafka.DeliveredOffer;
import com.comprigo.skyking.model.kafka.OfferRequest;
import com.comprigo.skyking.util.model.ApiUtils;
import com.comprigo.skyking.util.model.KafkaUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by stipe on 23/01/2017.
 */
public class ModelUtilsTestCase {

    @Test(expected = IllegalArgumentException.class)
    public void testValidateFlightsQuery() {
        FlightQuery flightsQuery = new FlightQuery();
        ApiUtils.validateFlightsQuery(flightsQuery);
    }

    @Test
    public void testConvertToOfferRequest() {
        FlightQuery flightsQuery = new FlightQuery("1", 1, "1", "from", "to", "20.01.17", "20.01.17", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36", "DE", "eurowings.com");
        OfferRequest offerRequest = KafkaUtils.convertToOfferRequest(flightsQuery, "1");

        Assert.assertEquals("1", offerRequest.getRequestId());
    }

    @Test
    public void testConvertToDeliveredOffer() {
        FlightResult flightResult = new FlightResult("from", "to", "fromCity", "toCity", "2017-01-20", "2017-01-20", "20", "71.3", "link");
        DeliveredOffer deliveredOffer = KafkaUtils.convertToDeliveredOffer(flightResult, "1");

        Assert.assertEquals("1", deliveredOffer.getRequestId());
    }

    @Test
    public void testPriceProcess() {
        Assert.assertEquals("71,60", ApiUtils.processPrice("71.6"));
        Assert.assertEquals("125,55", ApiUtils.processPrice("125.55"));
        Assert.assertEquals("22,00", ApiUtils.processPrice("22"));
        Assert.assertEquals("1150,20", ApiUtils.processPrice("1150.20"));
    }
}


