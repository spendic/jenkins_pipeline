package com.comprigo.skyking.util;

import com.comprigo.skyking.util.setup.IataCodeResolver;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Matija on 02/02/2017.
 */
public class IataCodeResolverTestCase {

    private static final Map<String, String> iataMap = Maps.newHashMap(
            ImmutableMap.<String, String>builder()
                    .put("PEK", "PEK")
                    .put("CDG", "CDG")
                    .put("DFW", "DFW")
                    .put("KUL", "KUL")
                    .put("PHX", "PHX")
                    .build()
    );
    private static final Map<String, String> destIataMap = Maps.newHashMap(
            ImmutableMap.<String, String>builder()
                    .put("Zagreb(ZAG)", "ZAG")
                    .put("Berlin (TXL)", "TXL")
                    .put("Berlin-Tegel(TXL)", "TXL")
                    .put("Beijing (PEK)", "PEK")
                    .put("Los Angeles International (LAX)", "LAX")
                    .put("Los Angeles (LAX)", "LAX")
                    .put("Hong Kong (HKG)", "HKG")
                    .put("Kuala Lumpur (KUL)", "KUL")
                    .put("Phoenix Sky Harbor (PHX)", "PHX")
                    .build()
    );
    private static final Map<String, String> destMap = Maps.newHashMap(
            ImmutableMap.<String, String>builder()
                    .put("Kroatien", "Kroatien")
                    .put("Split(Kroatien)", "SPU")
                    .put("Berlin-Tegel", "TXL")
                    .put("London (alle)", "LON")
                    .put("London Heathrow", "LHR")
                    .put("London Luton Airport", "LTN")
                    .put("Hartsfield Jackson Atlanta International", "ATL")
                    .put("Jackson Atlanta", "ATL")
                    .put("Beijing Capital International", "PEK")
                    .put("Dubai International", "DXB")
                    .put("Chicago O’Hare International", "ORD")
                    .put("Chicago", "CHI")
                    .put("Tokyo", "TYO")
                    .put("Los Angeles International", "LAX")
                    .put("Los Angeles", "LSQ")
                    .put("Hong Kong International Kai Tak", "HKG")
                    .put("Hong Kong Kai Tak", "HKG")
                    .put("Charles de Gaulle International", "CDG")
                    .put("Charles de Gaulle", "CDG")
                    .put("Dallas Fort Worth International", "DFW")
                    .put("Dallas", "DFW")
                    .put("DFW", "DFW")
                    .put("Kuala Lumpur International", "KUL")
                    .put("Kuala Lumpur", "KUL")
                    .put("Phoenix Sky Harbor International", "PHX")
                    .put("Phoenix Sky Harbor", "PHX")
                    .build()
    );
    private IataCodeResolver iataCodeResolver;

    @Before
    public void setUp() {
        iataCodeResolver = IataCodeResolver.getInstance();
    }

    @Test
    public void iataMapTest() {
        assertCase(iataMap, 100);
    }

    @Test
    public void destIataMapTest() {
        assertCase(destIataMap, 95);
    }

    @Test
    public void destMapTest() {
        assertCase(destMap, 90);
    }

    @Test
    public void allMapsTest() {
        Map<String, String> allMap = new HashMap<>();
        allMap.putAll(iataMap);
        allMap.putAll(destIataMap);
        allMap.putAll(destMap);
        assertCase(allMap, 90);
    }

    private void assertCase(Map<String, String> assertionMap, int minSuccessPercent) {
        List<String> matchList = new ArrayList<>();
        List<String> mismatchList = new ArrayList<>();

        assertionMap.entrySet().stream().forEach(o -> {
            final String iataCode = o.getValue();
            final String destinationStr = o.getKey();
            final String resolvedIataCode = iataCodeResolver.findIataCode(destinationStr);

            StringBuilder sb = new StringBuilder();
            if (StringUtils.equals(iataCode, resolvedIataCode)) {
                sb.append(createLog("Match", matchList, iataCode, destinationStr, resolvedIataCode));
                matchList.add(sb.toString());
            } else {
                sb.append(createLog("Mismatch", mismatchList, iataCode, destinationStr, resolvedIataCode));
                mismatchList.add(sb.toString());
            }
        });

        final int mapSize = assertionMap.entrySet().size();
        final int successCount = matchList.size();
        final int failureCount = mismatchList.size();
        final float percent = (float) successCount / mapSize * 100;

        System.out.println("\nTotal match number {" + successCount + "}");
        System.out.println("Total mismatch number {" + failureCount + "}");
        System.out.println("Success percent {" + percent + "} - required {" + minSuccessPercent + "}");

        matchList.stream().forEach(e -> System.out.println("\t" + e));
        System.out.println();
        mismatchList.stream().forEach(e -> System.out.println("\t" + e));

        Assert.assertTrue(percent >= minSuccessPercent);
    }

    private String createLog(String match, List<String> list, String iataCode, String destinationStr, String resolvedIataCode) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%s {%2d}", match, (list.size() + 1)))
                .append(String.format(" for destinationStr {%-50s}", destinationStr))
                .append(String.format(" - got {%-3s}", resolvedIataCode))
                .append(String.format(", expected {%-3s}", iataCode));
        return sb.toString();
    }
}
