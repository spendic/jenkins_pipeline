package com.comprigo.skyking.util;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created by stipe on 01/02/2017.
 */
@Ignore
public class DateUtilsTestCase {

    @Test
    public void testDatePatterns() {
        compareDates("Do 31 Jan", "2017-01-31", "De");
        compareDates("2017-01-31", "2017-01-31", "En");
        compareDates("01/31/2017", "2017-01-31", "En");
        compareDates("31.01.2017", "2017-01-31", "En");

        compareDates("FR., 27. Jan.", "2017-01-27", "En");
        compareDates("31 Jan 2017", "2017-01-31", "En");
        compareDates("31 January 2017", "2017-01-31", "En");
        compareDates("So. 31 Jan.", "2017-01-31", "En");

        compareDates("Friday 10 February 2017", "2017-02-10", "En");
        compareDates("on Tuesday 11 Apr", "2017-04-11", "En");
        compareDates("Mar 9", "2017-03-09", "En");
        compareDates("Tue 28 Feb 2017", "2017-02-28", "En");
        compareDates("Wed 15 Mar 17", "2017-03-15", "En");
        compareDates("Fri 24 Mar", "2017-03-24", "En");
        compareDates("Wed, Mar 01", "2017-03-01", "En");
        compareDates("Monday, 06 Mar 2017", "2017-03-06", "En");
        compareDates("01 Mar (Wed),", "2017-03-01", "En");
        compareDates("Wed, Mar 01", "2017-03-01", "En");
        compareDates("Mon. 03/13/2017", "2017-03-13", "En");
        compareDates("Sat 25 Mar", "2017-03-25", "De");
        compareDates("Dienstag, 7. März 2017", "2017-03-07", "De");
        compareDates("Mo., 13. März", "2017-03-13", "De");
    }

    @Test
    public void testBadDatePattern() {
        compareDates("bad date", "bad date", "En");
    }

    private void compareDates(String date, String expectedDate, String userCoutry) {
        String result = DateUtils.transformDate(date, userCoutry);
        Assert.assertEquals("For date: " + date, expectedDate, result);
    }
}
