package com.comprigo.skyking.util;

import com.comprigo.protobuf.flightcomparsion.Message;
import com.google.common.io.BaseEncoding;
import com.google.protobuf.InvalidProtocolBufferException;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by stipe on 08/02/2017.
 */
public class ClickServerClientTestCase {

    private static final String CLICK_SERVER_BASE_URL = "http://go.foxydeal.com";
    private ClickServerClient clickServerClient;

    private int pid = 10000;
    private String uid = "1300";
    private String subid = "1500";
    private String affiliateLink = "http://www.billigflieger.de/result?departure=&arrival=&dateDeparture=&dateReturn=&flightClass=&passengers=&tripType=&session=&client=";
    private String affiliateName = "billigflieger";
    private String domainName = "biligflieger.de";
    private String merchantCountry = "DE";
    private String userCountry = "DE";
    private String requestId = "6ere343-34-34-rr-ef-3-3f4reg";
    private String from = "CGN";
    private String to = "BER";
    private String departureDate = "2017-04-06";
    private String returnDate = "2017-04-06";
    private String price = "53";

    @Test
    public void testClickServerUrl() throws InvalidProtocolBufferException {
        clickServerClient = new ClickServerClient(CLICK_SERVER_BASE_URL);

        String clickServerUrl = clickServerClient.getFlightComparisonClickPathV1(pid, uid, subid, affiliateLink, affiliateName, domainName, merchantCountry, userCountry, requestId, from, to, departureDate, returnDate, price);

        String message = StringUtils.substringAfter(clickServerUrl, "?click=");
        Message.FlightClickMessage flightClickMessage = Message.FlightClickMessage.parseFrom(BaseEncoding.base64().decode(message));

        Assert.assertEquals(pid, flightClickMessage.getPid());
        Assert.assertEquals(uid, flightClickMessage.getUid());
        Assert.assertEquals(subid, flightClickMessage.getSubid());
        Assert.assertEquals(affiliateLink, flightClickMessage.getAffiliateLink());
        Assert.assertEquals(affiliateName, flightClickMessage.getAffiliateName());
        Assert.assertEquals(domainName, flightClickMessage.getDomainName());
        Assert.assertEquals(merchantCountry, flightClickMessage.getMerchantCountry());
        Assert.assertEquals(userCountry, flightClickMessage.getUserCountry());
        Assert.assertEquals(requestId, flightClickMessage.getRequestId());
        Assert.assertEquals(from, flightClickMessage.getFrom());
        Assert.assertEquals(to, flightClickMessage.getTo());
        Assert.assertEquals(departureDate, flightClickMessage.getDepartureDate());
        Assert.assertEquals(returnDate, flightClickMessage.getReturnDate());
        Assert.assertEquals(price, flightClickMessage.getPrice());
    }

    @Test
    public void testBadClickServerUrl() throws InvalidProtocolBufferException {
        clickServerClient = new ClickServerClient(CLICK_SERVER_BASE_URL);

        String clickServerUrl = clickServerClient.getFlightComparisonClickPathV1(null, uid, subid, affiliateLink, affiliateName, domainName, merchantCountry, userCountry, requestId, from, to, departureDate, returnDate, price);
        Assert.assertEquals(affiliateLink, clickServerUrl);
    }
}
