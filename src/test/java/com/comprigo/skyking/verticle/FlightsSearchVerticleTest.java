package com.comprigo.skyking.verticle;

import com.comprigo.skyking.model.api.FlightQuery;
import com.comprigo.skyking.model.api.FlightResult;
import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightResult;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.google.common.base.Charsets;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by stipe on 09/01/2017.
 */
public class FlightsSearchVerticleTest extends AbstractTestCase {

    private FlightQuery flightQuery = this.generateFlightQuery();

    private JsonObject authenticateResult = this.generateAuthenticationResult();
    private List<BilligfliegerFlightResult> billigfliegerFlightResults = Stream.generate(this::generateBilligfliegerFlightResult).limit(10).collect(Collectors.toList());

    @Test
    public void checkFlightResults(TestContext context) {

        wireMockRule.stubFor(WireMock.post(WireMock.urlMatching("/authenticate"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8;")
                        .withBody(authenticateResult.encodePrettily()))
        );

        wireMockRule.stubFor(WireMock.get(WireMock.urlMatching("/aggregations.*"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8;")
                        .withBody(Json.encode(billigfliegerFlightResults)))
        );

        final Async async = context.async();
        final String json = Json.encode(flightQuery);
        final String length = Integer.toString(json.getBytes(Charsets.UTF_8).length);

        vertx.createHttpClient().post(httpPort, "localhost", "/v1/flights")
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length)
                .handler(response -> {
                    context.assertEquals(response.statusCode(), 200);

                    response.bodyHandler(buffer -> {
                        List<FlightResult> results = Arrays.asList(Json.decodeValue(buffer.toString("utf-8"), FlightResult[].class));
                        context.assertEquals(results.size(), 10);
                        async.complete();
                    });
                })
                .write(json)
                .end();
    }

    @Test
    public void checkEmptyFlightResults(TestContext context) {

        wireMockRule.stubFor(WireMock.post(WireMock.urlMatching("/authenticate"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8;")
                        .withBody(authenticateResult.encodePrettily()))
        );

        wireMockRule.stubFor(WireMock.get(WireMock.urlMatching("/aggregations.*"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8;")
                        .withBody("[]"))
        );

        final Async async = context.async();
        final String json = Json.encode(flightQuery);
        final String length = Integer.toString(json.getBytes(Charsets.UTF_8).length);

        vertx.createHttpClient().post(httpPort, "localhost", "/v1/flights")
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length)
                .handler(response -> {
                    context.assertEquals(response.statusCode(), 200);

                    response.bodyHandler(buffer -> {
                        List<FlightResult> results = Arrays.asList(Json.decodeValue(buffer.toString("utf-8"), FlightResult[].class));
                        context.assertEquals(results.size(), 0);
                        async.complete();
                    });
                })
                .write(json)
                .end();

    }

    @Test
    public void checkBadFlightQuery(TestContext context) {

        wireMockRule.stubFor(WireMock.post(WireMock.urlMatching("/authenticate"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8;")
                        .withBody(authenticateResult.encodePrettily()))
        );

        wireMockRule.stubFor(WireMock.get(WireMock.urlMatching("/api/aggregations.*"))
                .willReturn(WireMock.aResponse()
                        .withStatus(400))
        );

        final Async async = context.async();
        final String json = "";
        final String length = Integer.toString(json.getBytes(Charsets.UTF_8).length);

        vertx.createHttpClient().post(httpPort, "localhost", "/v1/flights")
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length)
                .handler(response -> {
                    context.assertEquals(response.statusCode(), 400);
                    async.complete();
                })
                .write(json)
                .end();
    }

    @Test
    public void checkAuthentication(TestContext context) {

        wireMockRule.stubFor(WireMock.post(WireMock.urlMatching("/authenticate"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8;")
                        .withBody(authenticateResult.encodePrettily()))
        );

        wireMockRule.stubFor(WireMock.get(WireMock.urlMatching("/aggregations.*"))
                .withHeader("x-access-token", WireMock.containing(""))
                .willReturn(WireMock.aResponse()
                        .withStatus(401))
        );

        wireMockRule.stubFor(WireMock.get(WireMock.urlMatching("/aggregations.*"))
                .withHeader("x-access-token", WireMock.containing("token"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8;")
                        .withBody(Json.encode(billigfliegerFlightResults)))
        );

        final Async async = context.async();
        final String json = Json.encode(flightQuery);
        final String length = Integer.toString(json.getBytes(Charsets.UTF_8).length);

        vertx.createHttpClient().post(httpPort, "localhost", "/v1/flights")
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length)
                .handler(response -> {
                    context.assertEquals(response.statusCode(), 200);
                    async.complete();
                })
                .write(json)
                .end();
    }

    @Test
    public void checkBadAuthentication(TestContext context) {

        wireMockRule.stubFor(WireMock.post(WireMock.urlMatching("/authenticate"))
                .willReturn(WireMock.aResponse()
                        .withStatus(400))
        );

        wireMockRule.stubFor(WireMock.get(WireMock.urlMatching("/aggregations.*"))
                .willReturn(WireMock.aResponse()
                        .withStatus(401))
        );

        final Async async = context.async();
        final String json = Json.encode(flightQuery);
        final String length = Integer.toString(json.getBytes(Charsets.UTF_8).length);

        vertx.createHttpClient().post(httpPort, "localhost", "/v1/flights")
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length)
                .handler(response -> {
                    context.assertEquals(response.statusCode(), 400);
                    async.complete();
                })
                .write(json)
                .end();
    }
}