package com.comprigo.skyking.verticle;

import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightResult;
import com.github.tomakehurst.wiremock.client.WireMock;
import guru.nidi.ramltester.RamlDefinition;
import guru.nidi.ramltester.RamlLoaders;
import guru.nidi.ramltester.jaxrs.CheckingWebTarget;
import guru.nidi.ramltester.junit.RamlMatchers;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.TestContext;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Matija on 09/01/2017.
 */
public class RestEndpointsTestCase extends AbstractTestCase {

    private final RamlDefinition api = RamlLoaders.fromClasspath()
            .load("webroot/api/skyking.raml")
            .assumingBaseUri("http://skyking-st.herokuapp.com");

    private final ResteasyClient client = new ResteasyClientBuilder().build();
    private JsonObject authenticateResult = this.generateAuthenticationResult();
    private List<BilligfliegerFlightResult> billigfliegerFlightResults = Stream.generate(this::generateBilligfliegerFlightResult).limit(10).collect(Collectors.toList());
    private CheckingWebTarget checking;

    @Test
    public void testEndpoints(TestContext testContext) {

        wireMockRule.stubFor(WireMock.post(WireMock.urlMatching("/authenticate"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8;")
                        .withBody(authenticateResult.encodePrettily()))
        );

        wireMockRule.stubFor(WireMock.get(WireMock.urlMatching("/aggregations.*"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8;")
                        .withBody(Json.encode(billigfliegerFlightResults)))
        );

        checking = api.createWebTarget(client.target("http://Admin:AdminSkyking@localhost:" + httpPort));

        checking.path("/").request().get();
        Assert.assertThat(checking.getLastReport(), RamlMatchers.hasNoViolations());

        checking = api.createWebTarget(client.target("http://localhost:" + httpPort));

        checking.path("/health").request().get();
        Assert.assertThat(checking.getLastReport(), RamlMatchers.hasNoViolations());

        checking.path("/v1/flights").request().post(Entity.entity(Json.encode(generateFlightQuery()), MediaType.APPLICATION_JSON));
        Assert.assertThat(checking.getLastReport(), RamlMatchers.hasNoViolations());
    }
}