package com.comprigo.skyking.verticle;

import com.comprigo.skyking.model.api.FlightQuery;
import com.comprigo.skyking.model.api.FlightQueryBuilder;
import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightQuery;
import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightQueryBuilder;
import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightResult;
import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightResultBuilder;
import com.comprigo.skyking.verticle.kafka.KafkaVerticle;
import com.comprigo.skyking.verticle.search.BilligfliegerFlightsSearchVerticle;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.URISyntaxException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

/**
 * Created by stipe on 09/01/2017.
 */
@RunWith(VertxUnitRunner.class)
public abstract class AbstractTestCase {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(3030);

    protected Vertx vertx;
    protected int httpPort;

    @Before
    public void setUp(TestContext context) throws IOException, URISyntaxException {

        vertx = Vertx.vertx();

        httpPort = getPort();

        DeploymentOptions deploymentOptions = new DeploymentOptions().setConfig(
                new JsonObject()
                        .put("http.port", httpPort)
                        .put("auth.user", "user")
                        .put("auth.key", "key")
                        .put("session", "session")
                        .put("billigflieger.base.url", "http://localhost:3030")
                        .put("kafka.broker", "tcp://54.173.219.60:9092")
                        .put("kafka.topic", "skyking")
                        .put("kafka.key", "tracking")
                        .put("click.server.base.url", "http://clickserver-url")
        );

        HttpVerticle httpVerticle = Mockito.spy(new HttpVerticle());
        BilligfliegerFlightsSearchVerticle billigfliegerFlightsSearchVerticle = Mockito.spy(new BilligfliegerFlightsSearchVerticle());

        KafkaVerticle kafkaVerticle = Mockito.spy(new KafkaVerticle());
        doNothing().when(kafkaVerticle).reportOfferRequest(any());
        doNothing().when(kafkaVerticle).reportDeliveredOffer(any());

        vertx.deployVerticle(httpVerticle, deploymentOptions, context.asyncAssertSuccess());
        vertx.deployVerticle(billigfliegerFlightsSearchVerticle, deploymentOptions, context.asyncAssertSuccess());
        vertx.deployVerticle(kafkaVerticle, deploymentOptions.setWorker(true).setWorkerPoolSize(1).setWorkerPoolName("kafka-worker-1"), context.asyncAssertSuccess());
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    public FlightQuery generateFlightQuery() {
        return FlightQueryBuilder.aFlightQuery()
                .setUserId("_userId_")
                .setPartnerId(1300)
                .setPartnerSubId("_partnerSubId_")
                .setFrom("TEST")
                .setTo("TEST")
                .setDepartureDate("2016-06-20")
                .setReturnDate("2016-06-20")
                .setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36")
                .setUserCountry("DE")
                .setUrl("eurowings.com")
                .build();
    }

    public JsonObject generateAuthenticationResult() {
        JsonObject authResult = new JsonObject();
        authResult.put("success", true);
        authResult.put("message", "Token acquired");
        authResult.put("token", "token");

        return authResult;
    }

    public BilligfliegerFlightQuery generateBilligfliegerFlightQuery() {
        return BilligfliegerFlightQueryBuilder.aBilligfliegerFlightQuery()
                .setFrom("BER")
                .setTo("BCN")
                .setDepartureDate("2016-06-20")
                .setReturnDate("2016-06-20")
                .setMaxResults(10)
                .build();
    }

    public BilligfliegerFlightResult generateBilligfliegerFlightResult() {
        return BilligfliegerFlightResultBuilder.aBilligfliegerFlightResult()
                .setFrom("BER", "Berlin")
                .setTo("BCN", "Barcelona")
                .setDepartureDate("2016-06-20")
                .setReturnDate("2016-06-20")
                .setPriceFrom("200")
                .setAggregationAgeHours("2")
                .setDeepLink("http://devapptest.billigflieger.de/")
                .build();
    }

    public int getPort() throws IOException {

        ServerSocket socket = new ServerSocket(0);
        int port = socket.getLocalPort();
        socket.close();

        return port;
    }
}