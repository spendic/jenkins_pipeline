package com.comprigo.skyking.model.api;

/**
 * Created by stipe on 07/03/2017.
 */
public final class FlightQueryBuilder {
    private String userId;
    private Integer partnerId;
    private String partnerSubId;
    private String from;
    private String to;
    private String departureDate;
    private String returnDate;
    private Integer maxResults = 10; //default
    private String userAgent;
    private String userCountry;
    private String url;

    private FlightQueryBuilder() {
    }

    public static FlightQueryBuilder aFlightQuery() {
        return new FlightQueryBuilder();
    }

    public FlightQueryBuilder setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public FlightQueryBuilder setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
        return this;
    }

    public FlightQueryBuilder setPartnerSubId(String partnerSubId) {
        this.partnerSubId = partnerSubId;
        return this;
    }

    public FlightQueryBuilder setFrom(String from) {
        this.from = from;
        return this;
    }

    public FlightQueryBuilder setTo(String to) {
        this.to = to;
        return this;
    }

    public FlightQueryBuilder setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
        return this;
    }

    public FlightQueryBuilder setReturnDate(String returnDate) {
        this.returnDate = returnDate;
        return this;
    }

    public FlightQueryBuilder setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
        return this;
    }

    public FlightQueryBuilder setUserAgent(String userAgent) {
        this.userAgent = userAgent;
        return this;
    }

    public FlightQueryBuilder setUserCountry(String userCountry) {
        this.userCountry = userCountry;
        return this;
    }

    public FlightQueryBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public FlightQuery build() {
        FlightQuery flightQuery = new FlightQuery();
        flightQuery.setUserId(userId);
        flightQuery.setPartnerId(partnerId);
        flightQuery.setPartnerSubId(partnerSubId);
        flightQuery.setFrom(from);
        flightQuery.setTo(to);
        flightQuery.setDepartureDate(departureDate);
        flightQuery.setReturnDate(returnDate);
        flightQuery.setMaxResults(maxResults);
        flightQuery.setUserAgent(userAgent);
        flightQuery.setUserCountry(userCountry);
        flightQuery.setUrl(url);
        return flightQuery;
    }
}
