package com.comprigo.skyking.model.api;

/**
 * Created by stipe on 07/03/2017.
 */
public final class FlightResultBuilder {
    private String from;
    private String to;
    private String fromCity;
    private String toCity;
    private String departureDate;
    private String returnDate;
    private String aggregationAgeHours;
    private String priceFrom;
    private String clickServerLink;
    private String affiliateLink;

    private FlightResultBuilder() {
    }

    public static FlightResultBuilder aFlightResult() {
        return new FlightResultBuilder();
    }

    public FlightResultBuilder setFrom(String from) {
        this.from = from;
        return this;
    }

    public FlightResultBuilder setTo(String to) {
        this.to = to;
        return this;
    }

    public FlightResultBuilder setFromCity(String fromCity) {
        this.fromCity = fromCity;
        return this;
    }

    public FlightResultBuilder setToCity(String toCity) {
        this.toCity = toCity;
        return this;
    }

    public FlightResultBuilder setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
        return this;
    }

    public FlightResultBuilder setReturnDate(String returnDate) {
        this.returnDate = returnDate;
        return this;
    }

    public FlightResultBuilder setAggregationAgeHours(String aggregationAgeHours) {
        this.aggregationAgeHours = aggregationAgeHours;
        return this;
    }

    public FlightResultBuilder setPriceFrom(String priceFrom) {
        this.priceFrom = priceFrom;
        return this;
    }

    public FlightResultBuilder setAffiliateLink(String affiliateLink) {
        this.affiliateLink = affiliateLink;
        return this;
    }

    public FlightResultBuilder setClickServerLink(String clickServerLink) {
        this.clickServerLink = clickServerLink;
        return this;
    }

    public FlightResult build() {
        FlightResult flightResult = new FlightResult();
        flightResult.setFrom(from);
        flightResult.setTo(to);
        flightResult.setFromCity(fromCity);
        flightResult.setToCity(toCity);
        flightResult.setDepartureDate(departureDate);
        flightResult.setReturnDate(returnDate);
        flightResult.setAggregationAgeHours(aggregationAgeHours);
        flightResult.setPriceFrom(priceFrom);
        flightResult.setClickServerLink(clickServerLink);
        flightResult.setAffiliateLink(affiliateLink);
        return flightResult;
    }
}
