package com.comprigo.skyking.model.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by stipe on 04/01/2017.
 */
public class FlightQuery implements Serializable {

    @JsonProperty(value = "user_id")
    private String userId;

    @JsonProperty(value = "partner_id")
    private Integer partnerId;

    @JsonProperty(value = "partner_sub_id")
    private String partnerSubId;

    @JsonProperty(value = "from")
    private String from;

    @JsonProperty(value = "to")
    private String to;

    @JsonProperty(value = "departure_date")
    private String departureDate;

    @JsonProperty(value = "return_date")
    private String returnDate;

    @JsonProperty(value = "max_results")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer maxResults = 10; //default

    @JsonProperty(value = "user_agent")
    private String userAgent;

    @JsonProperty(value = "user_country")
    private String userCountry;

    @JsonProperty(value = "url")
    private String url;

    public FlightQuery() {
    }

    public FlightQuery(String userId, Integer partnerId, String partnerSubId, String from, String to, String departureDate, String returnDate, String userAgent, String userCountry, String url) {
        this.userId = userId;
        this.partnerId = partnerId;
        this.partnerSubId = partnerSubId;
        this.from = from;
        this.to = to;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.userAgent = userAgent;
        this.userCountry = userCountry;
        this.url = url;
    }

    public FlightQuery(String userId, Integer partnerId, String partnerSubId, String from, String to, String departureDate, String returnDate, Integer maxResults, String userAgent, String userCountry, String url) {
        this(userId, partnerId, partnerSubId, from, to, departureDate, returnDate, userAgent, userCountry, url);
        this.maxResults = maxResults;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerSubId() {
        return partnerSubId;
    }

    public void setPartnerSubId(String partnerSubId) {
        this.partnerSubId = partnerSubId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public Integer getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getUserCountry() {
        return userCountry;
    }

    public void setUserCountry(String userCountry) {
        this.userCountry = userCountry;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FlightQuery that = (FlightQuery) o;

        if (!userId.equals(that.userId)) {
            return false;
        }
        if (!partnerId.equals(that.partnerId)) {
            return false;
        }
        if (!partnerSubId.equals(that.partnerSubId)) {
            return false;
        }
        if (!from.equals(that.from)) {
            return false;
        }
        if (!to.equals(that.to)) {
            return false;
        }
        if (!departureDate.equals(that.departureDate)) {
            return false;
        }
        if (!returnDate.equals(that.returnDate)) {
            return false;
        }
        if (!userAgent.equals(that.userAgent)) {
            return false;
        }
        if (!userCountry.equals(that.userCountry)) {
            return false;
        }
        if (!url.equals(that.url)) {
            return false;
        }
        return maxResults != null ? maxResults.equals(that.maxResults) : that.maxResults == null;
    }

    @Override
    public int hashCode() {
        int result = userId.hashCode();
        result = 31 * result + partnerId.hashCode();
        result = 31 * result + partnerSubId.hashCode();
        result = 31 * result + from.hashCode();
        result = 31 * result + to.hashCode();
        result = 31 * result + departureDate.hashCode();
        result = 31 * result + returnDate.hashCode();
        result = 31 * result + (maxResults != null ? maxResults.hashCode() : 0);
        result = 31 * result + userAgent.hashCode();
        result = 31 * result + userCountry.hashCode();
        result = 31 * result + url.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "FlightQuery{" +
                "userId='" + userId + '\'' +
                ", partnerId='" + partnerId + '\'' +
                ", partnerSubId='" + partnerSubId + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", departureDate='" + departureDate + '\'' +
                ", returnDate='" + returnDate + '\'' +
                ", maxResults=" + maxResults +
                ", userAgent='" + userAgent + '\'' +
                ", userCountry='" + userCountry + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
