package com.comprigo.skyking.model.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Matija on 04/01/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlightResult implements Serializable {

    @JsonProperty(value = "from")
    private String from;

    @JsonProperty(value = "to")
    private String to;

    @JsonProperty(value = "fromCity")
    private String fromCity;

    @JsonProperty(value = "toCity")
    private String toCity;

    @JsonProperty(value = "departureDate")
    private String departureDate;

    @JsonProperty(value = "returnDate")
    private String returnDate;

    @JsonProperty(value = "aggregationAgeHours")
    private String aggregationAgeHours;

    @JsonProperty(value = "priceFrom")
    private String priceFrom;

    @JsonProperty(value = "redirectUrl")
    private String clickServerLink;

    @JsonIgnore
    private String affiliateLink;

    public FlightResult() {
    }

    public FlightResult(String from, String to, String fromCity, String toCity, String departureDate, String returnDate, String aggregationAgeHours, String priceFrom, String affiliateLink) {
        this.from = from;
        this.to = to;
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.aggregationAgeHours = aggregationAgeHours;
        this.priceFrom = priceFrom;
        this.affiliateLink = affiliateLink;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getAggregationAgeHours() {
        return aggregationAgeHours;
    }

    public void setAggregationAgeHours(String aggregationAgeHours) {
        this.aggregationAgeHours = aggregationAgeHours;
    }

    public String getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(String priceFrom) {
        this.priceFrom = priceFrom;
    }

    public String getAffiliateLink() {
        return affiliateLink;
    }

    public void setAffiliateLink(String affiliateLink) {
        this.affiliateLink = affiliateLink;
    }

    public String getClickServerLink() {
        return clickServerLink;
    }

    public void setClickServerLink(String clickServerLink) {
        this.clickServerLink = clickServerLink;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FlightResult)) return false;

        FlightResult that = (FlightResult) o;

        if (!from.equals(that.from)) return false;
        if (!to.equals(that.to)) return false;
        if (!fromCity.equals(that.fromCity)) return false;
        if (!toCity.equals(that.toCity)) return false;
        if (!departureDate.equals(that.departureDate)) return false;
        if (!returnDate.equals(that.returnDate)) return false;
        if (!aggregationAgeHours.equals(that.aggregationAgeHours)) return false;
        if (!priceFrom.equals(that.priceFrom)) return false;
        if (!clickServerLink.equals(that.clickServerLink)) return false;
        return affiliateLink.equals(that.affiliateLink);
    }

    @Override
    public int hashCode() {
        int result = from.hashCode();
        result = 31 * result + to.hashCode();
        result = 31 * result + fromCity.hashCode();
        result = 31 * result + toCity.hashCode();
        result = 31 * result + departureDate.hashCode();
        result = 31 * result + returnDate.hashCode();
        result = 31 * result + aggregationAgeHours.hashCode();
        result = 31 * result + priceFrom.hashCode();
        result = 31 * result + clickServerLink.hashCode();
        result = 31 * result + affiliateLink.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "FlightResult{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", fromCity='" + fromCity + '\'' +
                ", toCity='" + toCity + '\'' +
                ", departureDate='" + departureDate + '\'' +
                ", returnDate='" + returnDate + '\'' +
                ", aggregationAgeHours='" + aggregationAgeHours + '\'' +
                ", priceFrom='" + priceFrom + '\'' +
                ", clickServerLink='" + clickServerLink + '\'' +
                '}';
    }
}