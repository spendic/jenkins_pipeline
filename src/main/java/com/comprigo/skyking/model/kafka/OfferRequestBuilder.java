package com.comprigo.skyking.model.kafka;

import java.util.Date;

/**
 * Created by stipe on 06/03/2017.
 */
public final class OfferRequestBuilder {
    private String requestId;
    private String userId;
    private Integer partnerId;
    private String partnersubId;
    private String from;
    private String to;
    private String departureDate;
    private String returnDate;
    private Date requestDate;
    private String browser;
    private String browserVersion;
    private String urlPath;

    private OfferRequestBuilder() {
    }

    public static OfferRequestBuilder anOfferRequest() {
        return new OfferRequestBuilder();
    }

    public OfferRequestBuilder setRequestId(String requestId) {
        this.requestId = requestId;
        return this;
    }

    public OfferRequestBuilder setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public OfferRequestBuilder setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
        return this;
    }

    public OfferRequestBuilder setPartnersubId(String partnersubId) {
        this.partnersubId = partnersubId;
        return this;
    }

    public OfferRequestBuilder setFrom(String from) {
        this.from = from;
        return this;
    }

    public OfferRequestBuilder setTo(String to) {
        this.to = to;
        return this;
    }

    public OfferRequestBuilder setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
        return this;
    }

    public OfferRequestBuilder setReturnDate(String returnDate) {
        this.returnDate = returnDate;
        return this;
    }

    public OfferRequestBuilder setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
        return this;
    }

    public OfferRequestBuilder setBrowser(String browser) {
        this.browser = browser;
        return this;
    }

    public OfferRequestBuilder setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
        return this;
    }

    public OfferRequestBuilder setUrlPath(String urlPath) {
        this.urlPath = urlPath;
        return this;
    }

    public OfferRequest build() {
        OfferRequest offerRequest = new OfferRequest();
        offerRequest.setRequestId(requestId);
        offerRequest.setUserId(userId);
        offerRequest.setPartnerId(partnerId);
        offerRequest.setPartnersubId(partnersubId);
        offerRequest.setFrom(from);
        offerRequest.setTo(to);
        offerRequest.setDepartureDate(departureDate);
        offerRequest.setReturnDate(returnDate);
        offerRequest.setRequestDate(requestDate);
        offerRequest.setBrowser(browser);
        offerRequest.setBrowserVersion(browserVersion);
        offerRequest.setUrlPath(urlPath);
        return offerRequest;
    }
}
