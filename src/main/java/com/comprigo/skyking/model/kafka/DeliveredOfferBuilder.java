package com.comprigo.skyking.model.kafka;

import java.util.Date;

/**
 * Created by stipe on 06/03/2017.
 */
public final class DeliveredOfferBuilder {
    private String requestId;
    private String from;
    private String to;
    private String departureDate;
    private String returnDate;
    private String aggregationAgeHours;
    private String priceFrom;
    private String deepLink;
    private Date timestamp;
    private String offerOrigin;

    private DeliveredOfferBuilder() {
    }

    public static DeliveredOfferBuilder aDeliveredOffer() {
        return new DeliveredOfferBuilder();
    }

    public DeliveredOfferBuilder setRequestId(String requestId) {
        this.requestId = requestId;
        return this;
    }

    public DeliveredOfferBuilder setFrom(String from) {
        this.from = from;
        return this;
    }

    public DeliveredOfferBuilder setTo(String to) {
        this.to = to;
        return this;
    }

    public DeliveredOfferBuilder setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
        return this;
    }

    public DeliveredOfferBuilder setReturnDate(String returnDate) {
        this.returnDate = returnDate;
        return this;
    }

    public DeliveredOfferBuilder setAggregationAgeHours(String aggregationAgeHours) {
        this.aggregationAgeHours = aggregationAgeHours;
        return this;
    }

    public DeliveredOfferBuilder setPriceFrom(String priceFrom) {
        this.priceFrom = priceFrom;
        return this;
    }

    public DeliveredOfferBuilder setDeepLink(String deepLink) {
        this.deepLink = deepLink;
        return this;
    }

    public DeliveredOfferBuilder setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public DeliveredOfferBuilder setOfferOrigin(String offerOrigin) {
        this.offerOrigin = offerOrigin;
        return this;
    }

    public DeliveredOffer build() {
        DeliveredOffer deliveredOffer = new DeliveredOffer();
        deliveredOffer.setRequestId(requestId);
        deliveredOffer.setFrom(from);
        deliveredOffer.setTo(to);
        deliveredOffer.setDepartureDate(departureDate);
        deliveredOffer.setReturnDate(returnDate);
        deliveredOffer.setAggregationAgeHours(aggregationAgeHours);
        deliveredOffer.setPriceFrom(priceFrom);
        deliveredOffer.setDeepLink(deepLink);
        deliveredOffer.setTimestamp(timestamp);
        deliveredOffer.setOfferOrigin(offerOrigin);
        return deliveredOffer;
    }
}
