package com.comprigo.skyking.model.kafka;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by Matija on 25/01/2017.
 */
public class OfferRequest {

    @JsonProperty(value = "requestId")
    private String requestId;

    @JsonProperty(value = "userId")
    private String userId;

    @JsonProperty(value = "partnerId")
    private Integer partnerId;

    @JsonProperty(value = "partnersubId")
    private String partnersubId;

    @JsonProperty(value = "from")
    private String from;

    @JsonProperty(value = "to")
    private String to;

    @JsonProperty(value = "departureDate")
    private String departureDate;

    @JsonProperty(value = "returnDate")
    private String returnDate;

    @JsonProperty(value = "requestDate")
    private Date requestDate;

    @JsonProperty(value = "browser")
    private String browser;

    @JsonProperty(value = "browserVersion")
    private String browserVersion;

    @JsonProperty(value = "urlPath")
    private String urlPath;

    public OfferRequest() {
    }

    public OfferRequest(String uid, String userId, Integer partnerId, String partnersubId, String from, String to, String departureDate, String returnDate, Date requestDate, String browser, String browserVersion, String urlPath) {
        this.requestId = uid;
        this.userId = userId;
        this.partnerId = partnerId;
        this.partnersubId = partnersubId;
        this.from = from;
        this.to = to;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.requestDate = requestDate;
        this.browser = browser;
        this.browserVersion = browserVersion;
        this.urlPath = urlPath;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnersubId() {
        return partnersubId;
    }

    public void setPartnersubId(String partnersubId) {
        this.partnersubId = partnersubId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OfferRequest)) return false;

        OfferRequest that = (OfferRequest) o;

        if (!requestId.equals(that.requestId)) return false;
        if (!userId.equals(that.userId)) return false;
        if (!partnerId.equals(that.partnerId)) return false;
        if (!partnersubId.equals(that.partnersubId)) return false;
        if (!from.equals(that.from)) return false;
        if (!to.equals(that.to)) return false;
        if (!departureDate.equals(that.departureDate)) return false;
        if (!returnDate.equals(that.returnDate)) return false;
        if (!requestDate.equals(that.requestDate)) return false;
        if (!browser.equals(that.browser)) return false;
        if (!browserVersion.equals(that.browserVersion)) return false;
        return urlPath.equals(that.urlPath);
    }

    @Override
    public int hashCode() {
        int result = requestId.hashCode();
        result = 31 * result + userId.hashCode();
        result = 31 * result + partnerId.hashCode();
        result = 31 * result + partnersubId.hashCode();
        result = 31 * result + from.hashCode();
        result = 31 * result + to.hashCode();
        result = 31 * result + departureDate.hashCode();
        result = 31 * result + returnDate.hashCode();
        result = 31 * result + requestDate.hashCode();
        result = 31 * result + browser.hashCode();
        result = 31 * result + browserVersion.hashCode();
        result = 31 * result + urlPath.hashCode();
        return result;
    }
}
