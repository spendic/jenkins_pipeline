package com.comprigo.skyking.model.kafka;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by Matija on 25/01/2017.
 */
public class DeliveredOffer {

    @JsonProperty(value = "requestId")
    private String requestId;

    @JsonProperty(value = "from")
    private String from;

    @JsonProperty(value = "to")
    private String to;

    @JsonProperty(value = "departureDate")
    private String departureDate;

    @JsonProperty(value = "returnDate")
    private String returnDate;

    @JsonProperty(value = "aggregationAgeHours")
    private String aggregationAgeHours;

    @JsonProperty(value = "priceFrom")
    private String priceFrom;

    @JsonProperty(value = "deeplink")
    private String deepLink;

    @JsonProperty(value = "timestamp")
    private Date timestamp;

    @JsonProperty(value = "offerOrigin")
    private String offerOrigin;

    public DeliveredOffer() {
    }

    public DeliveredOffer(String uid, String from, String to, String departureDate, String returnDate, String aggregationAgeHours, String priceFrom, String deepLink, Date timestamp, String offerOrigin) {
        this.requestId = uid;
        this.from = from;
        this.to = to;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.aggregationAgeHours = aggregationAgeHours;
        this.priceFrom = priceFrom;
        this.deepLink = deepLink;
        this.timestamp = timestamp;
        this.offerOrigin = offerOrigin;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getAggregationAgeHours() {
        return aggregationAgeHours;
    }

    public void setAggregationAgeHours(String aggregationAgeHours) {
        this.aggregationAgeHours = aggregationAgeHours;
    }

    public String getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(String priceFrom) {
        this.priceFrom = priceFrom;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public void setDeepLink(String deepLink) {
        this.deepLink = deepLink;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getOfferOrigin() {
        return offerOrigin;
    }

    public void setOfferOrigin(String offerOrigin) {
        this.offerOrigin = offerOrigin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeliveredOffer)) return false;

        DeliveredOffer that = (DeliveredOffer) o;

        if (!requestId.equals(that.requestId)) return false;
        if (!from.equals(that.from)) return false;
        if (!to.equals(that.to)) return false;
        if (!departureDate.equals(that.departureDate)) return false;
        if (!returnDate.equals(that.returnDate)) return false;
        if (!aggregationAgeHours.equals(that.aggregationAgeHours)) return false;
        if (!priceFrom.equals(that.priceFrom)) return false;
        if (deepLink != null ? !deepLink.equals(that.deepLink) : that.deepLink != null) return false;
        if (!timestamp.equals(that.timestamp)) return false;
        return offerOrigin.equals(that.offerOrigin);
    }

    @Override
    public int hashCode() {
        int result = requestId.hashCode();
        result = 31 * result + from.hashCode();
        result = 31 * result + to.hashCode();
        result = 31 * result + departureDate.hashCode();
        result = 31 * result + returnDate.hashCode();
        result = 31 * result + aggregationAgeHours.hashCode();
        result = 31 * result + priceFrom.hashCode();
        result = 31 * result + (deepLink != null ? deepLink.hashCode() : 0);
        result = 31 * result + timestamp.hashCode();
        result = 31 * result + offerOrigin.hashCode();
        return result;
    }
}
