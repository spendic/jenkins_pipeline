package com.comprigo.skyking.model.billigflieger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by stipe on 06/03/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BilligfliegerFlightResult {

    @JsonProperty(value = "from")
    private Destination from;

    @JsonProperty(value = "to")
    private Destination to;

    @JsonProperty(value = "departureDate")
    private String departureDate;

    @JsonProperty(value = "returnDate")
    private String returnDate;

    @JsonProperty(value = "aggregationAgeHours")
    private String aggregationAgeHours;

    @JsonProperty(value = "priceFrom")
    private String priceFrom;

    @JsonProperty(value = "deeplink")
    private String deepLink;

    public BilligfliegerFlightResult() {
    }

    public Destination getFrom() {
        return from;
    }

    public void setFrom(Destination from) {
        this.from = from;
    }

    public Destination getTo() {
        return to;
    }

    public void setTo(Destination to) {
        this.to = to;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getAggregationAgeHours() {
        return aggregationAgeHours;
    }

    public void setAggregationAgeHours(String aggregationAgeHours) {
        this.aggregationAgeHours = aggregationAgeHours;
    }

    public String getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(String priceFrom) {
        this.priceFrom = priceFrom;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public void setDeepLink(String deepLink) {
        this.deepLink = deepLink;
    }

    @Override
    public String toString() {
        return "BilligfliegerFlightResult{" +
                "from=" + from +
                ", to=" + to +
                ", departureDate='" + departureDate + '\'' +
                ", returnDate='" + returnDate + '\'' +
                ", aggregationAgeHours='" + aggregationAgeHours + '\'' +
                ", priceFrom='" + priceFrom + '\'' +
                ", deepLink='" + deepLink + '\'' +
                '}';
    }
}