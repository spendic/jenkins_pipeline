package com.comprigo.skyking.model.billigflieger;

/**
 * Created by stipe on 06/03/2017.
 */
public final class BilligfliegerFlightResultBuilder {

    private Destination from;
    private Destination to;
    private String departureDate;
    private String returnDate;
    private String aggregationAgeHours;
    private String priceFrom;
    private String deepLink;

    private BilligfliegerFlightResultBuilder() {
    }

    public static BilligfliegerFlightResultBuilder aBilligfliegerFlightResult() {
        return new BilligfliegerFlightResultBuilder();
    }

    public BilligfliegerFlightResultBuilder setFrom(String iataCode, String cityName) {
        this.from = new Destination(iataCode, cityName);
        return this;
    }

    public BilligfliegerFlightResultBuilder setTo(String iataCode, String cityName) {
        this.to = new Destination(iataCode, cityName);
        return this;
    }

    public BilligfliegerFlightResultBuilder setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
        return this;
    }

    public BilligfliegerFlightResultBuilder setReturnDate(String returnDate) {
        this.returnDate = returnDate;
        return this;
    }

    public BilligfliegerFlightResultBuilder setAggregationAgeHours(String aggregationAgeHours) {
        this.aggregationAgeHours = aggregationAgeHours;
        return this;
    }

    public BilligfliegerFlightResultBuilder setPriceFrom(String priceFrom) {
        this.priceFrom = priceFrom;
        return this;
    }

    public BilligfliegerFlightResultBuilder setDeepLink(String deepLink) {
        this.deepLink = deepLink;
        return this;
    }

    public BilligfliegerFlightResult build() {
        BilligfliegerFlightResult billigfliegerFlightResult = new BilligfliegerFlightResult();
        billigfliegerFlightResult.setFrom(from);
        billigfliegerFlightResult.setTo(to);
        billigfliegerFlightResult.setDepartureDate(departureDate);
        billigfliegerFlightResult.setReturnDate(returnDate);
        billigfliegerFlightResult.setAggregationAgeHours(aggregationAgeHours);
        billigfliegerFlightResult.setPriceFrom(priceFrom);
        billigfliegerFlightResult.setDeepLink(deepLink);
        return billigfliegerFlightResult;
    }
}
