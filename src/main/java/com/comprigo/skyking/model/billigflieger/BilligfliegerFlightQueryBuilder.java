package com.comprigo.skyking.model.billigflieger;

/**
 * Created by stipe on 06/03/2017.
 */
public final class BilligfliegerFlightQueryBuilder {
    private String from;
    private String to;
    private String departureDate;
    private String returnDate;
    private Integer maxResults;
    private String sessionId;

    private BilligfliegerFlightQueryBuilder() {
    }

    public static BilligfliegerFlightQueryBuilder aBilligfliegerFlightQuery() {
        return new BilligfliegerFlightQueryBuilder();
    }

    public BilligfliegerFlightQueryBuilder setFrom(String from) {
        this.from = from;
        return this;
    }

    public BilligfliegerFlightQueryBuilder setTo(String to) {
        this.to = to;
        return this;
    }

    public BilligfliegerFlightQueryBuilder setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
        return this;
    }

    public BilligfliegerFlightQueryBuilder setReturnDate(String returnDate) {
        this.returnDate = returnDate;
        return this;
    }

    public BilligfliegerFlightQueryBuilder setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
        return this;
    }

    public BilligfliegerFlightQuery build() {
        BilligfliegerFlightQuery billigfliegerFlightQuery = new BilligfliegerFlightQuery();
        billigfliegerFlightQuery.setFrom(from);
        billigfliegerFlightQuery.setTo(to);
        billigfliegerFlightQuery.setDepartureDate(departureDate);
        billigfliegerFlightQuery.setReturnDate(returnDate);
        billigfliegerFlightQuery.setMaxResults(maxResults);
        return billigfliegerFlightQuery;
    }
}
