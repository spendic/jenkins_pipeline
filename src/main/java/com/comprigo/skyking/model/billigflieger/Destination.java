package com.comprigo.skyking.model.billigflieger;

/**
 * Created by stipe on 9.3.2017..
 */
public class Destination {
    private String iata;
    private String cityName;

    public Destination() {
    }

    public Destination(String iata, String cityName) {
        this.iata = iata;
        this.cityName = cityName;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public String toString() {
        return "Destination{" +
                "iata='" + iata + '\'' +
                ", cityName='" + cityName + '\'' +
                '}';
    }
}