package com.comprigo.skyking.util.model;

import com.comprigo.skyking.model.api.FlightQuery;
import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightQuery;
import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightQueryBuilder;

/**
 * Created by stipe on 06/03/2017.
 */
public class BilligfliegerUtils {

    public static BilligfliegerFlightQuery convertToBilligfliegerFlightQuery(FlightQuery flightQuery) {
        return BilligfliegerFlightQueryBuilder.aBilligfliegerFlightQuery()
                .setTo(flightQuery.getTo())
                .setFrom(flightQuery.getFrom())
                .setDepartureDate(flightQuery.getDepartureDate())
                .setReturnDate(flightQuery.getReturnDate())
                .setMaxResults(flightQuery.getMaxResults())
                .build();
    }
}
