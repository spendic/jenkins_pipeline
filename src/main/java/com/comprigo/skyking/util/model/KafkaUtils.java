package com.comprigo.skyking.util.model;

import com.comprigo.skyking.model.api.FlightQuery;
import com.comprigo.skyking.model.api.FlightResult;
import com.comprigo.skyking.model.kafka.DeliveredOffer;
import com.comprigo.skyking.model.kafka.DeliveredOfferBuilder;
import com.comprigo.skyking.model.kafka.OfferRequest;
import com.comprigo.skyking.model.kafka.OfferRequestBuilder;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * Created by stipe on 06/03/2017.
 */
public class KafkaUtils {

    static private UserAgentStringParser parser = UADetectorServiceFactory.getResourceModuleParser();

    public static OfferRequest convertToOfferRequest(FlightQuery flightsQuery, String uid) {

        String browser = "";
        String browserVersion = "";
        if (StringUtils.isNotBlank(flightsQuery.getUserAgent())) {
            ReadableUserAgent userAgent = parser.parse(flightsQuery.getUserAgent());
            browser = userAgent.getFamily().getName();
            browserVersion = userAgent.getVersionNumber().getMajor();
        }

        return OfferRequestBuilder.anOfferRequest()
                .setRequestId(uid)
                .setUserId(flightsQuery.getUserId())
                .setPartnerId(flightsQuery.getPartnerId())
                .setPartnersubId(flightsQuery.getPartnerSubId())
                .setFrom(flightsQuery.getFrom())
                .setTo(flightsQuery.getTo())
                .setDepartureDate(flightsQuery.getDepartureDate())
                .setReturnDate(flightsQuery.getReturnDate())
                .setRequestDate(new Date())
                .setUrlPath(flightsQuery.getUrl())
                .setBrowser(browser)
                .setBrowserVersion(browserVersion)
                .build();
    }

    public static DeliveredOffer convertToDeliveredOffer(FlightResult flightResult, String uid) {
        return DeliveredOfferBuilder.aDeliveredOffer()
                .setRequestId(uid)
                .setFrom(flightResult.getFrom())
                .setTo(flightResult.getTo())
                .setDepartureDate(flightResult.getDepartureDate())
                .setReturnDate(flightResult.getReturnDate())
                .setAggregationAgeHours(flightResult.getAggregationAgeHours())
                .setPriceFrom(flightResult.getPriceFrom())
                .setDeepLink(flightResult.getAffiliateLink())
                .setTimestamp(new Date())
                .setOfferOrigin("api")
                .build();
    }
}
