package com.comprigo.skyking.util.model;

import com.comprigo.skyking.model.api.FlightQuery;
import com.comprigo.skyking.model.api.FlightResult;
import com.comprigo.skyking.model.api.FlightResultBuilder;
import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightResult;
import com.comprigo.skyking.util.ClickServerClient;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Created by Matija on 12/01/2017.
 */
public class ApiUtils {

    public static void validateFlightsQuery(FlightQuery flightsQuery) {

        Preconditions.checkArgument(!StringUtils.isBlank(flightsQuery.getUserId()), "User id must not be null");
        Preconditions.checkArgument(flightsQuery.getPartnerId() != null, "Parner id must not be null");
        Preconditions.checkArgument(!StringUtils.isBlank(flightsQuery.getPartnerSubId()), "Partner sub id must not be null");
        Preconditions.checkArgument(!StringUtils.isBlank(flightsQuery.getFrom()), "From must not be null");
        Preconditions.checkArgument(!StringUtils.isBlank(flightsQuery.getTo()), "To must not be null");
        Preconditions.checkArgument(!StringUtils.isBlank(flightsQuery.getDepartureDate()), "Departure date must not be null");
        Preconditions.checkArgument(!StringUtils.isBlank(flightsQuery.getReturnDate()), "Return date must not be null");
        Preconditions.checkArgument(!StringUtils.isBlank(flightsQuery.getUserAgent()), "User country must not be null");
        Preconditions.checkArgument(!StringUtils.isBlank(flightsQuery.getUrl()), "Url must not be null");
    }

    public static FlightResult convertToFlightResult(BilligfliegerFlightResult billigfliegerFlightResult) {
        return FlightResultBuilder.aFlightResult()
                .setFrom(billigfliegerFlightResult.getFrom().getIata())
                .setTo(billigfliegerFlightResult.getTo().getIata())
                .setFromCity(billigfliegerFlightResult.getFrom().getCityName())
                .setToCity(billigfliegerFlightResult.getTo().getCityName())
                .setDepartureDate(billigfliegerFlightResult.getDepartureDate())
                .setReturnDate(billigfliegerFlightResult.getReturnDate())
                .setAggregationAgeHours(billigfliegerFlightResult.getAggregationAgeHours())
                .setAffiliateLink(billigfliegerFlightResult.getDeepLink())
                .setPriceFrom(billigfliegerFlightResult.getPriceFrom())
                .build();
    }

    public static FlightResult processFlightResult(ClickServerClient clickServerClient, FlightQuery flightQuery, FlightResult flightResult, String requestId) {

        String defaultCountry = "DE";
        String defaultAffiliateName = "billigflieger";

        flightResult.setClickServerLink(
                clickServerClient.getFlightComparisonClickPathV1(
                        flightQuery.getPartnerId(),
                        flightQuery.getUserId(),
                        flightQuery.getPartnerSubId(),
                        flightResult.getAffiliateLink(),
                        defaultAffiliateName,
                        flightQuery.getUrl(),
                        defaultCountry,
                        flightQuery.getUserCountry(),
                        requestId,
                        flightResult.getFrom(),
                        flightResult.getTo(),
                        flightResult.getDepartureDate(),
                        flightResult.getReturnDate(),
                        flightResult.getPriceFrom()
                )
        );

        flightResult.setFromCity(StringUtils.isEmpty(flightResult.getFromCity()) ? flightResult.getFrom() : flightResult.getFromCity());
        flightResult.setToCity(StringUtils.isEmpty(flightResult.getToCity()) ? flightResult.getFrom() : flightResult.getToCity());
        flightResult.setPriceFrom(processPrice(flightResult.getPriceFrom()));

        return flightResult;
    }

    public static String processPrice(String priceFrom) {
        try {
            BigDecimal formatedPrice = new BigDecimal(priceFrom);

            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
            symbols.setGroupingSeparator(' ');
            symbols.setDecimalSeparator(',');

            DecimalFormat formatter = new DecimalFormat("#.00", symbols);
            return formatter.format(formatedPrice);

        } catch (NumberFormatException e) {
            return priceFrom;
        }
    }
}
