package com.comprigo.skyking.util;

import com.joestelmach.natty.Parser;
import org.apache.commons.lang3.StringUtils;

import java.text.DateFormatSymbols;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by stipe on 01/02/2017.
 */
public class DateUtils {

    public static String transformDate(String date, String userCountry) throws DateTimeException {

        Optional<String> dateOptional = transformDateWithDatePattern(date);
        if (!dateOptional.isPresent()) {
            dateOptional = transformDateWithNatty(date);
        }

        if (!dateOptional.isPresent()) {
            String translatedDate = translateDate(date, userCountry);
            dateOptional = transformDateWithNatty(translatedDate);
        }

        return dateOptional.orElse(date);
    }

    private static Optional<String> transformDateWithDatePattern(String date) {

        Optional<String> optionalDate = Optional.empty();

        try {
            DateTimeFormatter resultDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            DateTimeFormatter dateFormatter = new DateTimeFormatterBuilder()
                    .appendPattern("[yyyy-MM-dd]")
                    .appendPattern("[yyyy/MM/dd]")
                    .appendPattern("[dd.MM.yyyy]")
                    .toFormatter();

            LocalDate localDate = LocalDate.parse(date, dateFormatter);
            optionalDate = Optional.of(resultDate.format(localDate));
        } catch (DateTimeParseException e) {
        }

        return optionalDate;
    }

    private static Optional<String> transformDateWithNatty(String date) {

        DateTimeFormatter resultDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Parser parser = new Parser();

        Optional<String> optionalDate = parser
                .parse(date)
                .stream()
                .findFirst()
                .flatMap(dateGroup -> dateGroup.getDates().stream().findFirst()
                        .map(parsedDate -> {
                            Instant instant = parsedDate.toInstant();
                            ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
                            LocalDate localDate = zdt.toLocalDate();
                            return resultDate.format(localDate);
                        })
                );

        return optionalDate;
    }

    private static String translateDate(String date, String userCountry) {
        try {
            Locale locale = new Locale(userCountry);
            DateFormatSymbols localeDateFormatSymbols = new DateFormatSymbols(locale);

            List<String> localeDays = Arrays.asList(localeDateFormatSymbols.getWeekdays());
            List<String> localeMonths = Arrays.asList(localeDateFormatSymbols.getMonths());

            DateFormatSymbols defaultDateFormatSymbols = new DateFormatSymbols(new Locale("En"));
            List<String> defaultDays = Arrays.asList(defaultDateFormatSymbols.getWeekdays());
            List<String> defaultMonths = Arrays.asList(defaultDateFormatSymbols.getMonths());

            Map<String, String> days = IntStream.range(0, localeDays.size())
                    .boxed()
                    .collect(Collectors.toMap(i -> localeDays.get(i).toString().toLowerCase(), i -> defaultDays.get(i).toString().toLowerCase()));

            Map<String, String> months = IntStream.range(0, Math.min(localeMonths.size(), defaultMonths.size()))
                    .boxed()
                    .collect(Collectors.toMap(i -> localeMonths.get(i).toString().toLowerCase(), i -> defaultMonths.get(i).toString().toLowerCase()));

            String translatedDate = date.toLowerCase();

            for (Map.Entry<String, String> entry : days.entrySet()) {
                translatedDate = StringUtils.replace(translatedDate, entry.getKey(), entry.getValue());
            }

            for (Map.Entry<String, String> entry : months.entrySet()) {
                translatedDate = StringUtils.replace(translatedDate, entry.getKey(), entry.getValue());
            }

            return translatedDate;
        } catch (Exception e) {
            return date;
        }
    }
}
