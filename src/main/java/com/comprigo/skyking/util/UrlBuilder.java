package com.comprigo.skyking.util;

import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightQuery;
import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;

/**
 * Created by stipe on 10/01/2017.
 */
public class UrlBuilder {

    private static final String AUTHENTICATE_PATH = "/authenticate";
    private static final String FLIGHT_SEARCH_PATH = "/aggregations";
    private static final String FROM = "from";
    private static final String TO = "to";
    private static final String DEPARTURE_DATE = "departureDate";
    private static final String RETURN_DATE = "returnDate";
    private static final String MAX_RESULTS = "maxResults";
    private static final String DEVIATION_DAYS = "deviationDays";
    private static final String SESSION_ID = "sessionId";

    public static String buildBilligfliegerFlightSearchRequest(String billigfliegerBaseUrl, String session, BilligfliegerFlightQuery billigfliegerFlightQuery) throws URISyntaxException {

        URIBuilder uriBuilder = new URIBuilder(billigfliegerBaseUrl + FLIGHT_SEARCH_PATH)
                .addParameter(FROM, billigfliegerFlightQuery.getFrom())
                .addParameter(TO, billigfliegerFlightQuery.getTo())
                .addParameter(DEPARTURE_DATE, billigfliegerFlightQuery.getDepartureDate())
                .addParameter(RETURN_DATE, billigfliegerFlightQuery.getReturnDate())
                .addParameter(MAX_RESULTS, Integer.toString(billigfliegerFlightQuery.getMaxResults()))
                .addParameter(DEVIATION_DAYS, "10")
                .addParameter(SESSION_ID, session);

        return uriBuilder.build().toString();
    }

    public static String buildBilligfliegerAuthenticateRequest(String billigfliegerBaseUrl) throws URISyntaxException {

        URIBuilder uriBuilder = new URIBuilder(billigfliegerBaseUrl + AUTHENTICATE_PATH);
        return uriBuilder.build().toString();
    }
}
