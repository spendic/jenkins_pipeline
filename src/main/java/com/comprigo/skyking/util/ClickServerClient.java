package com.comprigo.skyking.util;

import com.comprigo.protobuf.flightcomparsion.Message;
import com.google.common.io.BaseEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClickServerClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClickServerClient.class);
    private static final String FLIGHT_SEARCH_CLICK_PATH = "/v1/flightcomparison";

    private final String clickServerBaseUrl;

    public ClickServerClient(String clickServerBaseUrl) {
        this.clickServerBaseUrl = clickServerBaseUrl;
    }

    /**
     * @param pid
     * @param uid
     * @param subid
     * @param affiliateLink
     * @param domainName
     * @param requestId
     * @param from
     * @param to
     * @param departureDate
     * @param returnDate
     * @param price
     * @return /v1/flightcomparison?click=CJBOEgQxMzAwGgQxNT...
     */
    public String getFlightComparisonClickPathV1(Integer pid, String uid, String subid, String affiliateLink, String affiliateName, String domainName, String merchantCountry, String userCountry, String requestId, String from, String to, String departureDate, String returnDate, String price) {
        try {
            Message.FlightClickMessage flightClickMessage = Message.FlightClickMessage.newBuilder()
                    .setPid(pid)
                    .setUid(uid)
                    .setSubid(subid)
                    .setAffiliateLink(affiliateLink)
                    .setAffiliateName(affiliateName)
                    .setDomainName(domainName)
                    .setMerchantCountry(merchantCountry)
                    .setUserCountry(userCountry)
                    .setRequestId(requestId)
                    .setFrom(from)
                    .setTo(to)
                    .setDepartureDate(departureDate)
                    .setReturnDate(returnDate)
                    .setPrice(price)
                    .build();

            String message = BaseEncoding.base64().encode(flightClickMessage.toByteArray());
            return new StringBuilder().append(clickServerBaseUrl).append(FLIGHT_SEARCH_CLICK_PATH).append("?click=").append(message).toString();
        } catch (Exception e) {
            LOGGER.warn("Click server redirect url build failed: {}", e.getMessage());
            return affiliateLink;
        }
    }
}