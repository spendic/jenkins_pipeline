package com.comprigo.skyking.util.setup;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by stipe on 10.3.2017..
 */
public class BasicAuth implements AuthProvider {

    private static final String USERNAME = "Admin";
    private static final String PASSWORD = "AdminSkyking";

    @Override
    public void authenticate(JsonObject jsonObject, Handler<AsyncResult<User>> handler) {

        Boolean isAuthenticated = StringUtils.equals(jsonObject.getString("username"), USERNAME) && StringUtils.equals(jsonObject.getString("password"), PASSWORD);

        handler.handle(new AsyncResult<User>() {
            @Override
            public User result() {
                return null;
            }

            @Override
            public Throwable cause() {
                return null;
            }

            @Override
            public boolean succeeded() {
                return isAuthenticated;
            }

            @Override
            public boolean failed() {
                return !isAuthenticated;
            }
        });
    }
}