package com.comprigo.skyking.util.setup;

import com.newrelic.api.agent.NewRelic;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

public class NewrelicMetricHandler implements Handler<RoutingContext> {

    public static final String TRACKING = "test";

    @Override
    public void handle(RoutingContext context) {
        NewRelic.setTransactionName(TRACKING, context.normalisedPath());
        context.next();
    }
}
