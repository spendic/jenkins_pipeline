package com.comprigo.skyking.util.setup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.commons.io.IOUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Created by stipe on 01/02/2017.
 */
public class IataCodeResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(IataCodeResolver.class);

    private static final String LUCENE_DIR_PATH = "lucene-directory";
    private static final String IATA_JSON_PATH = "iata.json";

    private static IataCodeResolver instance = new IataCodeResolver();

    private Directory directory;
    private IndexWriter indexWriter;

    private IataCodeResolver() {
        try {
            directory = FSDirectory.open(Paths.get(LUCENE_DIR_PATH));
            IndexWriterConfig indexWriterConfig = new IndexWriterConfig(new StandardAnalyzer());
            indexWriter = new IndexWriter(directory, indexWriterConfig);

            List<Airport> airports = decodeIataJson(IATA_JSON_PATH);
            indexDocuments(indexWriter, airports);

        } catch (URISyntaxException | IOException e) {
            LOGGER.warn(e.getMessage());
        } finally {
            IOUtils.closeQuietly(indexWriter);
        }
    }

    public static IataCodeResolver getInstance() {
        return instance;
    }

    private List<Airport> decodeIataJson(String jsonFilePath) throws URISyntaxException, IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(jsonFilePath);
        String content = new String(IOUtils.toByteArray(inputStream), Charset.defaultCharset());

        return Arrays.asList(Json.decodeValue(content, Airport[].class));
    }

    private void indexDocuments(IndexWriter indexWriter, List<Airport> airports) throws IOException {
        for (Airport airport : airports) {
            Document document = new Document();

            document.add(new StoredField("iata", airport.getIataCode(), TextField.TYPE_STORED));
            document.add(new StoredField("airportName", airport.getAirportName(), TextField.TYPE_STORED));

            indexWriter.addDocument(document);
        }
    }

    public String findIataCode(String destination) {

        String iataCode = null;
        DirectoryReader directoryReader = null;

        try {
            directoryReader = DirectoryReader.open(directory);
            IndexSearcher indexSearcher = new IndexSearcher(directoryReader);

            QueryParser iataParser = new QueryParser("iata", new StandardAnalyzer());
            Query iataQuery = iataParser.parse(destination);

            QueryParser airportNameParser = new QueryParser("airportName", new StandardAnalyzer());
            Query airportNameQuery = airportNameParser.parse(destination);

            BooleanQuery booleanQuery = new BooleanQuery.Builder()
                    .add(iataQuery, BooleanClause.Occur.SHOULD)
                    .add(airportNameQuery, BooleanClause.Occur.SHOULD)
                    .build();

            TopDocs topDocs = indexSearcher.search(booleanQuery, 10);
            List<ScoreDoc> scoreDocs = Arrays.asList(topDocs.scoreDocs);

            if (!scoreDocs.isEmpty()) {
                iataCode = indexSearcher.doc(scoreDocs.get(0).doc).getField("iata").stringValue();
            }
        } catch (IOException | ParseException e) {
            LOGGER.warn(e.getMessage());
        } finally {
            IOUtils.closeQuietly(directoryReader);
        }

        return iataCode != null ? iataCode : destination;
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
class Airport implements Serializable {

    @JsonProperty(value = "code")
    private String iataCode;

    @JsonProperty(value = "name")
    private String airportName;

    public String getIataCode() {
        return iataCode;
    }

    public void setIataCode(String iataCode) {
        this.iataCode = iataCode;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }
}
