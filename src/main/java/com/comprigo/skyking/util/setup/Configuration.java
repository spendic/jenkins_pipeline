package com.comprigo.skyking.util.setup;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * Created by stipe on 04/01/2017.
 */
public class Configuration {

    public Integer getKafkaWorker() {
        return Integer.parseInt(System.getenv("kafka.workers"));
    }

    public Integer getKafkaWorkerPool() {
        return Integer.parseInt(System.getenv("kafka.worker.pool"));
    }

    public Map getConfig() {
        return ImmutableMap.builder()

                .put("http.port", Integer.parseInt(System.getProperty("http.port", "8080")))

                .put("auth.user", System.getenv("auth.user"))
                .put("auth.key", System.getenv("auth.key"))

                .put("session", System.getenv("session"))
                .put("billigflieger.base.url", System.getenv("billigflieger.base.url"))

                .put("kafka.broker", System.getenv("kafka.broker"))
                .put("flight.query.topic", System.getenv("flight.query.topic"))
                .put("flight.query.key", System.getenv("flight.query.key"))
                .put("flight.results.topic", System.getenv("flight.results.topic"))
                .put("flight.results.key", System.getenv("flight.results.key"))

                .put("kafka.workers", getKafkaWorker())
                .put("kafka.worker.pool", getKafkaWorkerPool())

                .put("click.server.base.url", System.getenv("click.server.base.url"))
                .build();
    }

    @Override
    public String toString() {
        return getConfig().toString();
    }
}
