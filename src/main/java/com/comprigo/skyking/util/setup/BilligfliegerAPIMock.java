package com.comprigo.skyking.util.setup;

import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightResult;
import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightResultBuilder;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Matija on 17/01/2017.
 */
public class BilligfliegerAPIMock {

    private static final Integer PORT = 3031;
    private WireMockServer wireMockServer;

    private JsonObject authenticateResult = this.generateAuthenticationResult();
    private List<BilligfliegerFlightResult> billigfliegerFlightResults = Stream.generate(this::generateBilligfliegerFlightResult).limit(10).collect(Collectors.toList());

    public void start() {
        wireMockServer = new WireMockServer(PORT);
        wireMockServer.start();

        final WireMock wireMock = new WireMock(wireMockServer.port());
        wireMock.register(WireMock.post(WireMock.urlMatching("/authenticate"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8;")
                        .withBody(authenticateResult.encodePrettily())
                        .withUniformRandomDelay(50, 1000)));

        wireMock.register(WireMock.get(WireMock.urlMatching("/aggregations.*"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8;")
                        .withBody(Json.encode(billigfliegerFlightResults))
                        .withUniformRandomDelay(50, 1000)));
    }

    public void stop() {
        wireMockServer.stop();
    }

    public JsonObject generateAuthenticationResult() {
        JsonObject authResult = new JsonObject();
        authResult.put("success", true);
        authResult.put("message", "Token acquired");
        authResult.put("token", "token");

        return authResult;
    }

    public BilligfliegerFlightResult generateBilligfliegerFlightResult() {
        return BilligfliegerFlightResultBuilder.aBilligfliegerFlightResult()
                .setFrom("BER", "Berlin")
                .setTo("BCN", "Barcelona")
                .setDepartureDate("2017-06-20")
                .setReturnDate("2017-06-20")
                .setPriceFrom("200")
                .setAggregationAgeHours("2")
                .setDeepLink("http://devapptest.billigflieger.de/")
                .build();
    }
}
