package com.comprigo.skyking.verticle;

import com.comprigo.skyking.model.api.FlightQuery;
import com.comprigo.skyking.model.api.FlightResult;
import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightQuery;
import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightResult;
import com.comprigo.skyking.model.kafka.DeliveredOffer;
import com.comprigo.skyking.model.kafka.OfferRequest;
import com.comprigo.skyking.util.ClickServerClient;
import com.comprigo.skyking.util.DateUtils;
import com.comprigo.skyking.util.model.ApiUtils;
import com.comprigo.skyking.util.model.BilligfliegerUtils;
import com.comprigo.skyking.util.model.KafkaUtils;
import com.comprigo.skyking.util.setup.BasicAuth;
import com.comprigo.skyking.util.setup.IataCodeResolver;
import com.comprigo.skyking.util.setup.NewrelicMetricHandler;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.AuthHandler;
import io.vertx.ext.web.handler.BasicAuthHandler;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import org.bson.types.ObjectId;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by stipe on 04/01/2017.
 */
public class HttpVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpVerticle.class);

    private static final String RAML_ENDPOINT = "/";
    private static final String HEALTH_ENDPOINT = "/health";
    private static final String FLIGHTS_QUERY_ENDPOINT = "/v1/flights";

    private HttpServer httpServer;
    private IataCodeResolver iataCodeResolver;
    private ClickServerClient clickServerClient;

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);

        vertx.executeBlocking(future -> {
            iataCodeResolver = IataCodeResolver.getInstance();
            clickServerClient = new ClickServerClient(context.config().getString("click.server.base.url"));
            future.complete();
        }, res -> {
        });

        LOGGER.info("Http verticle deployment complete");
    }

    @Override
    public void start(Future<Void> future) {

        Router router = Router.router(vertx);

        router.route().handler(new NewrelicMetricHandler());

        AuthHandler basicAuthHandler = BasicAuthHandler.create(new BasicAuth());
        router.route(RAML_ENDPOINT).handler(basicAuthHandler);

        router.route(FLIGHTS_QUERY_ENDPOINT + "*").handler(BodyHandler.create());
        router.post(FLIGHTS_QUERY_ENDPOINT).handler(this::sendFlightsQuery);

        router.get(HEALTH_ENDPOINT).handler(this::healthCheck);

        router.route().handler(StaticHandler.create());

        httpServer = vertx.createHttpServer();
        httpServer.requestHandler(router::accept)
                .listen(
                        config().getInteger("http.port"),
                        result -> {
                            if (result.succeeded()) {
                                future.complete();
                            } else {
                                future.fail(result.cause());
                            }
                        }
                );
    }

    @Override
    public void stop(Future<Void> stopFuture) {
        httpServer.close();
        stopFuture.complete();
        LOGGER.info("Http verticle shutdown complete");
    }

    private void sendFlightsQuery(RoutingContext routingContext) {
        try {
            FlightQuery flightQuery = Json.decodeValue(routingContext.getBodyAsString(), FlightQuery.class);
            ApiUtils.validateFlightsQuery(flightQuery);

            final String from = iataCodeResolver == null ? flightQuery.getFrom() : iataCodeResolver.findIataCode(flightQuery.getFrom());
            final String to = iataCodeResolver == null ? flightQuery.getTo() : iataCodeResolver.findIataCode(flightQuery.getTo());

            flightQuery.setFrom(from);
            flightQuery.setTo(to);

            flightQuery.setDepartureDate(DateUtils.transformDate(flightQuery.getDepartureDate(), flightQuery.getUserCountry()));
            flightQuery.setReturnDate(DateUtils.transformDate(flightQuery.getReturnDate(), flightQuery.getUserCountry()));

            final String requestId = new ObjectId().toHexString();

            sendFlightQueryToKafka(flightQuery, requestId);

            BilligfliegerFlightQuery billigfliegerFlightQuery = BilligfliegerUtils.convertToBilligfliegerFlightQuery(flightQuery);
            Future<Message<String>> billigfliegerMessage = Future.future();
            vertx.eventBus().send("billigfliegerFlightQuery", Json.encode(billigfliegerFlightQuery), billigfliegerMessage.completer());

            CompositeFuture.all(Arrays.asList(billigfliegerMessage)).setHandler(asyncResult -> {
                if (asyncResult.succeeded()) {

                    Message<String> message = asyncResult.result().resultAt(0);
                    List<FlightResult> flightResults = Arrays.asList(Json.decodeValue(message.body(), BilligfliegerFlightResult[].class))
                            .stream()
                            .map(billigfliegerFlightResult -> ApiUtils.convertToFlightResult(billigfliegerFlightResult))
                            .map(flightResult -> ApiUtils.processFlightResult(clickServerClient, flightQuery, flightResult, requestId))
                            .collect(Collectors.toList());

                    LOGGER.info("Flight search result reply: {}", flightResults);

                    if (!flightResults.isEmpty()) {
                        sendFlightResultsToKafka(flightResults, requestId);
                    }

                    Integer maxResults = flightQuery.getMaxResults() < 1 ? 1 : flightQuery.getMaxResults();
                    flightResults = flightResults.stream().limit(maxResults).collect(Collectors.toList());

                    routingContext.response()
                            .setStatusCode(200)
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .end(Json.encodePrettily(flightResults));

                } else {
                    routingContext.response().setStatusCode(400).end();
                }

            });
        } catch (DecodeException | IllegalArgumentException e) {
            LOGGER.warn(e.getMessage());
            routingContext.response().setStatusCode(400).end();
        }
    }

    private void sendFlightQueryToKafka(FlightQuery flightsQuery, String requestId) {

        LOGGER.info("Sending flight query to kafka: {}", flightsQuery);
        OfferRequest offerRequest = KafkaUtils.convertToOfferRequest(flightsQuery, requestId);
        vertx.eventBus().send("reportOfferRequest", Json.encode(offerRequest));
    }

    private void sendFlightResultsToKafka(List<FlightResult> flightsResults, String requestId) {

        LOGGER.info("Sending flight results to kafka: {}", flightsResults.size());

        List<DeliveredOffer> deliveredOffers = flightsResults.stream()
                .map(flightResult -> KafkaUtils.convertToDeliveredOffer(flightResult, requestId))
                .collect(Collectors.toList());

        vertx.eventBus().send("reportDeliveredOffer", Json.encode(deliveredOffers));
    }

    private void healthCheck(RoutingContext routingContext) {
        routingContext.response().putHeader("content-type", "text/plain").end("UP");
    }
}
