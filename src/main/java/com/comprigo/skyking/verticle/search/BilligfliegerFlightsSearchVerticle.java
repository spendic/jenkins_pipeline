package com.comprigo.skyking.verticle.search;

import com.comprigo.skyking.model.billigflieger.BilligfliegerFlightQuery;
import com.comprigo.skyking.util.UrlBuilder;
import com.google.common.base.Charsets;
import com.newrelic.api.agent.Trace;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.net.URISyntaxException;

/**
 * Created by stipe on 10/01/2017.
 */
public class BilligfliegerFlightsSearchVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(BilligfliegerFlightsSearchVerticle.class);

    private static final String BILLIGFLIEGER_API_METRIC = "billigflieger_api";

    private HttpClient httpClient;
    private MessageConsumer<String> flightQueryConsumer;

    private String authToken;
    private String billigfliegerBaseUrl;
    private String session;

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);

        HttpClientOptions options = new HttpClientOptions();
        httpClient = vertx.createHttpClient(options);

        authToken = context.config().getString("auth.token", null);
        billigfliegerBaseUrl = context.config().getString("billigflieger.base.url");
        session = context.config().getString("session");

        LOGGER.info("Flight search verticle deployment complete");
    }

    @Override
    public void start(Future<Void> future) {
        flightQueryConsumer = vertx.eventBus().consumer("billigfliegerFlightQuery", this::getBilligfliegerFlightResults);
        future.complete();
    }

    @Override
    public void stop(Future<Void> stopFuture) {
        httpClient.close();
        flightQueryConsumer.unregister();
        stopFuture.complete();

        LOGGER.info("Flight search verticle shutdown complete");
    }

    @Trace(metricName = BILLIGFLIEGER_API_METRIC, dispatcher = true, leaf = true)
    private void getBilligfliegerFlightResults(Message<String> message) {

        if (authToken == null) {
            LOGGER.warn("AuthToken is null - required authentication");
            authenticate(message);
        } else {
            try {
                final BilligfliegerFlightQuery billigfliegerFlightQuery = Json.decodeValue(message.body(), BilligfliegerFlightQuery.class);
                final String url = UrlBuilder.buildBilligfliegerFlightSearchRequest(billigfliegerBaseUrl, session, billigfliegerFlightQuery);
                LOGGER.info("Flight query {} encoded as url: {}", billigfliegerFlightQuery, url);

                HttpClientRequest request = httpClient.requestAbs(HttpMethod.GET, url);

                request.handler(response -> {

                    if (response.statusCode() == 200) {
                        response.bodyHandler(totalBuffer -> {
                            message.reply(totalBuffer.toString("utf-8"));
                        });
                    } else if (response.statusCode() == 401) {
                        LOGGER.info("HTTP status code {} for url {} and query {} in billigflieger search", response.statusCode(), url, billigfliegerFlightQuery);
                        authenticate(message);
                    } else {
                        LOGGER.warn("HTTP status code {} for url {} and query {} in billigflieger search", response.statusCode(), url, billigfliegerFlightQuery);
                        message.fail(1, "Error code: " + response.statusCode());
                    }

                });

                request.exceptionHandler(e -> {
                    LOGGER.warn("Request exception: {} in billigflieger search", e.getMessage());
                    message.fail(1, e.getMessage());
                });

                request.putHeader("x-access-token", authToken);
                request.end();

            } catch (URISyntaxException | DecodeException e) {
                LOGGER.warn(e.getMessage());
                message.fail(1, e.getMessage());
            }
        }
    }

    private void authenticate(Message<String> message) {
        try {
            final String url = UrlBuilder.buildBilligfliegerAuthenticateRequest(billigfliegerBaseUrl);

            JsonObject authRequest = new JsonObject();
            authRequest.put("user", context.config().getString("auth.user"));
            authRequest.put("key", context.config().getString("auth.key"));

            HttpClientRequest request = httpClient.postAbs(url);

            request.handler(response -> {
                if (response.statusCode() == 200) {

                    response.bodyHandler(totalBuffer -> {

                        JsonObject authResult = new JsonObject(totalBuffer.toString());
                        authToken = authResult.getString("token");

                        context.config().put("auth.token", authToken);
                        LOGGER.info("Auth token generated: {}", authToken);

                        getBilligfliegerFlightResults(message);
                    });

                } else {
                    LOGGER.warn("HTTP status code {} for url {} and query {} in billigflieger auth", response.statusCode(), url, authRequest.toString());
                    message.fail(1, "" + response.statusCode());
                }
            });

            request.exceptionHandler(e -> {
                LOGGER.warn("Request exception: {} in billigflieger auth", e.getMessage());
                message.fail(1, e.getMessage());
            });

            request.putHeader("Content-Type", "application/json");
            request.putHeader("Content-Length", Integer.toString(authRequest.toString().getBytes(Charsets.UTF_8).length));

            request.write(authRequest.encode());
            request.end();

        } catch (URISyntaxException e) {
            LOGGER.warn(e.getMessage());
            message.fail(1, e.getMessage());
        }
    }
}