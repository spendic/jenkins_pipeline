package com.comprigo.skyking.verticle.kafka;

import com.comprigo.skyking.model.kafka.DeliveredOffer;
import com.comprigo.skyking.model.kafka.OfferRequest;
import com.foxydeal.cepler.input.api.Reporter;
import com.foxydeal.cepler.input.api.ReporterFactory;
import com.foxydeal.cepler.input.kafka.KafkaConfiguration;
import com.newrelic.api.agent.Trace;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Matija on 19/01/2017.
 */
public class KafkaVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaVerticle.class);

    private static final String OFFER_REQUEST_METRIC = "offer_request";
    private static final String OFFER_RESPONSE_METRIC = "offer_response";

    private Reporter reporter;
    private MessageConsumer<String> flightQueryConsumer;
    private MessageConsumer<String> flightResultsConsumer;

    private String flightQueryTopic;
    private String flightQueryKey;

    private String flightResultsTopic;
    private String flightResultsKey;

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);

        KafkaConfiguration kafkaConfiguration = KafkaConfiguration.builder().broker(config().getString("kafka.broker")).build();
        reporter = ReporterFactory.createKafkaReporter(kafkaConfiguration);

        flightQueryTopic = config().getString("flight.query.topic");
        flightQueryKey = config().getString("flight.query.key");

        flightResultsTopic = config().getString("flight.results.topic");
        flightResultsKey = config().getString("flight.results.key");

        LOGGER.info("Kafka verticle deployment complete");
    }

    @Override
    public void start(Future<Void> future) {
        flightQueryConsumer = vertx.eventBus().consumer("reportOfferRequest", this::reportOfferRequest);
        flightResultsConsumer = vertx.eventBus().consumer("reportDeliveredOffer", this::reportDeliveredOffer);

        future.complete();
    }

    @Override
    public void stop(Future<Void> stopFuture) {
        reporter.close();
        flightQueryConsumer.unregister();
        flightResultsConsumer.unregister();
        stopFuture.complete();

        LOGGER.info("Kafka verticle shutdown");
    }

    @Trace(metricName = OFFER_REQUEST_METRIC, dispatcher = true, leaf = true)
    public void reportOfferRequest(Message<String> message) {
        try {
            OfferRequest offerRequest = Json.decodeValue(message.body(), OfferRequest.class);
            LOGGER.info("Received offer request with id {}", offerRequest.getRequestId());

            reporter.send(flightQueryTopic, flightQueryKey, offerRequest);

        } catch (Exception e) {
            LOGGER.warn("Exception in reporting offer request: {}", e.getMessage());
            message.fail(1, e.getMessage());
        }

        message.reply("ok");
    }

    @Trace(metricName = OFFER_RESPONSE_METRIC, dispatcher = true, leaf = true)
    public void reportDeliveredOffer(Message<String> message) {
        try {
            List<DeliveredOffer> deliveredOffers = Arrays.asList(Json.decodeValue(message.body(), DeliveredOffer[].class));
            LOGGER.info("Received {} flights", deliveredOffers.size());

            deliveredOffers.stream().forEach(deliveredOffer -> {
                reporter.send(flightResultsTopic, flightResultsKey, deliveredOffer);
            });

        } catch (Exception e) {
            LOGGER.warn("Exception in reporting delivered offers: {}", e.getMessage());
            message.fail(1, e.getMessage());
        }

        message.reply("ok");
    }
}
