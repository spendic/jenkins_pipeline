package com.comprigo.skyking;

import com.comprigo.skyking.util.setup.BilligfliegerAPIMock;
import com.comprigo.skyking.util.setup.Configuration;
import com.comprigo.skyking.verticle.HttpVerticle;
import com.comprigo.skyking.verticle.kafka.KafkaVerticle;
import com.comprigo.skyking.verticle.search.BilligfliegerFlightsSearchVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.logging.SLF4JLogDelegateFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.util.stream.IntStream;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Created by stipe on 04/01/2017.
 */
public class Starter {

    private static final Logger LOGGER = getLogger(Starter.class);

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        System.setProperty(LoggerFactory.LOGGER_DELEGATE_FACTORY_CLASS_NAME, SLF4JLogDelegateFactory.class.getName());

        Configuration configuration = new Configuration();
        LOGGER.info("{}", configuration.toString());

        DeploymentOptions deploymentOptions = new DeploymentOptions().setConfig(new JsonObject(configuration.getConfig()));

        VertxOptions vertxOptions = new VertxOptions();
        Vertx vertx = Vertx.vertx(vertxOptions);

        vertx.deployVerticle(HttpVerticle.class.getName(), deploymentOptions);
        vertx.deployVerticle(BilligfliegerFlightsSearchVerticle.class.getName(), deploymentOptions);

        //If billigflieger.base.url configuration variable contains "localhost" - start billigflieger http mocking
        if (StringUtils.contains((String) configuration.getConfig().get("billigflieger.base.url"), "localhost")) {
            BilligfliegerAPIMock billigfliegerHttpMock = new BilligfliegerAPIMock();
            billigfliegerHttpMock.start();
        }

        //Kafka initialization
        IntStream.rangeClosed(1, configuration.getKafkaWorker())
                .forEach(i -> vertx.deployVerticle(
                        KafkaVerticle.class.getName(),
                        deploymentOptions.setWorker(true)
                                .setWorkerPoolSize(configuration.getKafkaWorkerPool())
                                .setWorkerPoolName("kafka-worker-" + i)
                        )
                );
    }
}
